# mediatek-x554l
Repositorio de drivers para los ordenadores Asus X554L con Ubuntu y otros ordenadores que usen el chip híbrido MediaTek MT7630E.
-----------------------------------------------
INSTRUCCIONES DE USO
-----------------------------------------------
Este driver ha sido modificado para funcionar como un módulo. En un principio, sólo es compatible con Ubuntu / Mate, estoy trabajando para poder usarlo en otras plataformas. Antes de compilar, por favor, comprueba que tienes todas las dependencias para poder instalar módulos. Si las tienes, este es el proceso:

Primero: Obten los archivos

git clone git@github.com:pjpmosteiro/mediatek-x554l.git
cd mt7630e

Segundo: Compilar los drivers:

make

Si la compilación se completó como debiera, deberian haber dos módulos: rt2x00/mt7630e.ko and btloader/mt76xx.ko. Para poder instalar el firmware y los drivers, es necesario ser root, preferible su a sudo, pero dejo sudo por si acaso:

sudo -s make install

Cargamos los drivers:

sudo modprobe mt7630e
sudo modprobe mt76xx

A partir de ahora, se supone que el sistema cargará los módulos durante el arranque.

PARA DESINSTALAR, ejecuta lo siguiente:

cd path_to_mt7630e/
sudo -s make uninstall

(ALPHA, mejor no lo hagas) También es posible usar el driver sin instalarlo, pero es mejor que lo instales por estabilidad. Primero copia el firmware:

sudo cp -v firmware/*/* /lib/firmware/

y carga manualmente los drivers:

sudo modprobe rt2800pci
sudo insmod ./rt2x00/mt7630e.ko
sudo insmod ./btloader/mt76xx.ko

Nota: cargar rt2800pci es necesario para asegurarnos de que todos los módulos están cargados correctamente. Puedes usar modprobe -r rt2800pci para quitar los módulos extra luego de que mt7630e esté cargado.

También es posible habilitar la compilación dinámica de módulos con el paquete dkms:

sudo make dkms

De esta forma, los drivers se cargaran siempre desde el arranque.
